package ar.com.imperium.test.misc;

import com.google.common.base.Splitter;

public class StringSplitterTest {

	public static void main(String[] args) {
		testStringSplitter();
	}

	private static void testStringSplitter() {
		String sample = "create, read, update, remove";
		Iterable<String> answer = Splitter.on(",").trimResults()
				.omitEmptyStrings().split(sample);

		for (String string : answer) {
			System.out.println("The answer is :" + string);
		}
	}
}
