/**
 * 
 */
package ar.com.imperium.test.common.security;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ar.com.imperium.common.security.IHashService;
import ar.com.imperium.common.security.Sha1Hasher;

public class Sha512HashTest
{
    private static final Logger logger = LoggerFactory
        .getLogger(Sha512HashTest.class);

    public static void main(String[] args)
    {
        try {
            Sha512HashTest.test();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void test() throws Exception
    {
        IHashService hashService = new Sha1Hasher();

        List<String> passwordList = new ArrayList<String>();
        for (int i = 0; i < 20; i++) {
            passwordList.add("password" + i);
        }
        String hashed;
        for (String eachString : passwordList) {
            hashed = hashService.hashString(eachString);
            logger.debug("<string, hash>" + eachString + "," + hashed);
        }

    }

}
