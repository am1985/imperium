/**
 * 
 */
package ar.com.imperium.test.json;

import com.google.common.base.Objects;

/**
 * @author user
 * 
 */
public class EmbeddedObject
{
    private Integer id;

    private SampleObject sampleObject;

    public EmbeddedObject(Integer id)
    {
        this.id = id;
    }

    public SampleObject getSampleObject()
    {
        return sampleObject;
    }

    public void setSampleObject(SampleObject sampleObject)
    {
        this.sampleObject = sampleObject;
    }

    public String toString()
    {
        return Objects
            .toStringHelper(getClass())
            .add("id", id)
            .add("sampleObject", sampleObject.toString())
            .toString();

    }
}
