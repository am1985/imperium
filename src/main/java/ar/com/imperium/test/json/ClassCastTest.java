/**
 * 
 */
package ar.com.imperium.test.json;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ar.com.imperium.common.PrimitiveTypes;

/**
 * @author user
 * 
 */
public class ClassCastTest
{
    public static void main(String[] args)
    {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("number", "12");
        map.put("string", "stt");
        map.put("double", "12.2");

        Map<String, PrimitiveTypes> params =
            new HashMap<String, PrimitiveTypes>();
        params.put("number", PrimitiveTypes.INTEGER_TYPE);
        params.put("string", PrimitiveTypes.STRING_TYPE);
        params.put("double", PrimitiveTypes.FLOAT_TYPE);

        Set<String> keys = params.keySet();
        for (String eachKey : keys) {

        }
    }
}
