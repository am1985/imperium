/**
 * 
 */
package ar.com.imperium.test.json;

import java.util.List;

import com.google.common.base.Objects;

/**
 * @author user
 * 
 */
public class SampleObject
{

    private boolean status;

    private String description;

    private List<String> stringList;

    public SampleObject()
    {
    }

    public SampleObject(boolean status, String description,
        List<String> stringList)
    {
        super();
        this.status = status;
        this.description = description;
        this.stringList = stringList;
    }

    public String toString()
    {
        return Objects
            .toStringHelper(getClass())
            .add("status", status)
            .add("description", description)
            .add("stringList", stringList.toString())
            .toString();
    }
}
