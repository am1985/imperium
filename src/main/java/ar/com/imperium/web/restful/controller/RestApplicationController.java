/**
 * 
 */
package ar.com.imperium.web.restful.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import ar.com.imperium.common.IPropertiesHelper;
import ar.com.imperium.service.interfaces.IApplicationService;

@Controller
public class RestApplicationController
{

    private static final Logger logger = LoggerFactory
        .getLogger(RestApplicationController.class);

    @Autowired
    @Qualifier("jpaComplexApplicationService")
    private IApplicationService applicationService;

    @Autowired
    @Qualifier("applicationPropertiesHelper")
    private IPropertiesHelper propertiesHelper;

    private Map<String, Object> getDefaultValues()
    {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(
            "page",
            new Integer(propertiesHelper.getProperty("default.page")));
        map.put(
            "pageSize",
            new Integer(propertiesHelper.getProperty("default.pageSize")));
        map.put(
            "sort",
            propertiesHelper.getProperty("application.default.sort.order"));

        return map;
    }

}
