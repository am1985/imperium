package ar.com.imperium.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.imperium.service.interfaces.IApiService;
import ar.com.imperium.service.interfaces.IApplicationService;
import ar.com.imperium.service.interfaces.IPermissionService;
import ar.com.imperium.service.interfaces.IRoleService;
import ar.com.imperium.service.interfaces.ISubjectService;

@Service("apiServiceImpl")
@Transactional
public class ApiServiceImpl implements IApiService
{

    private static final Logger logger = LoggerFactory
        .getLogger(ApiServiceImpl.class);

    /**
     * Application service
     */
    // @Autowired
    // @Qualifier("jpaComplexApplicationService")
    // private IApplicationService applicationService;

    /**
     * Permission service
     */
    @Autowired
    @Qualifier("jpaComplexPermissionService")
    private IPermissionService permissionService;

    /**
     * Subject service
     */
    @Autowired
    @Qualifier("jpaComplexSubjectService")
    private ISubjectService subjectService;

    @Autowired
    @Qualifier("jpaComplexRoleService")
    private IRoleService roleService;

}
