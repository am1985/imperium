/**
 * 
 */
package ar.com.imperium.service.interfaces;

import java.util.List;
import java.util.Map;

import ar.com.imperium.domain.Application;
import ar.com.imperium.exception.ImperiumException;

/**
 * @author user
 * 
 */
public interface IService<T, ID>
{
    public List<T> findAll(Integer page, Integer maxSize, String sortCriteria)
        throws ImperiumException;

    public Long findTotal();

    public List<T> findAllWithDetail(Integer page, Integer maxSize,
        String sortCriteria) throws ImperiumException;

    public T findById(Long id) throws ImperiumException;

    public T findWithDetailById(Long id) throws ImperiumException;

    public List<T> create(List<T> entityList) throws ImperiumException;

    public T create(T entityData) throws ImperiumException;

    public T update(T entity) throws ImperiumException;

    public T update(ID id, Map<String, Object> newValues) throws Exception;

    public void delete(T entity) throws ImperiumException;

    public List<T> findAllForApplication(Application application, Integer page,
        Integer maxSize, String sortCriteria) throws ImperiumException;

    public List<T> findAllForApplication(Application application, Integer page,
        Integer maxSize, String sortCriteria, String direction)
        throws ImperiumException;

    public List<T> findAllForApplicationWhere(Application application,
        Map<String, Object> queryParams, Map<String, Object> pagination,
        Map<String, Object> order);

    public Long findQtyForApplication(Application application)
        throws ImperiumException;

    public Long findQtyForApplicationWhere(Application application,
        Map<String, Object> queryParams);

    public List<T> findWithIds(List<ID> idList);

}
