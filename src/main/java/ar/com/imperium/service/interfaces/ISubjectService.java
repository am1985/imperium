/**
 * 
 */
package ar.com.imperium.service.interfaces;

import java.util.List;
import java.util.Map;

import ar.com.imperium.domain.Application;
import ar.com.imperium.domain.Role;
import ar.com.imperium.domain.Subject;

/**
 * @author user
 * 
 */
public interface ISubjectService extends IService<Subject, Long>
{
    public void addToApplication(Application application,
        List<Subject> subjectList) throws Exception;

    public List<Subject> findSubjectsForRole(Map<String, Object> queryParams,
        Map<String, Object> pagination, Map<String, Object> order)
        throws Exception;

    public Long findQtyForSubjectsForRole(Map<String, Object> queryParams)
        throws Exception;

    public List<Subject> findAvailableForRoleInApplication(
        Map<String, Object> queryParams, Map<String, Object> pagination,
        Map<String, Object> order) throws Exception;

    public Long findQtyAvailableForRoleInApplication(
        Map<String, Object> queryParams, Map<String, Object> pagination,
        Map<String, Object> order) throws Exception;

    public Map<String, Object> updateRoles(Long subjectId, List<Role> toAdd,
        List<Role> toRemove) throws Exception;

    public Map<String, Object> updateRoles(Subject subject, List<Long> toAdd,
        List<Long> toRemove) throws Exception;

    /**
     * For each role return a long with the subject qty where the role is being
     * used
     * 
     * @param roleList
     * @return
     * @throws Exception
     */
    public Map<Long, Long> findSubjectQtyForRole(List<Role> roleList)
        throws Exception;
}
