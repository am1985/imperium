/**
 * 
 */
package ar.com.imperium.service.interfaces;

import java.util.List;
import java.util.Map;

import ar.com.imperium.domain.Application;
import ar.com.imperium.domain.Permission;
import ar.com.imperium.domain.Role;
import ar.com.imperium.domain.Subject;
import ar.com.imperium.exception.ImperiumException;

/**
 * @author user
 * 
 */
public interface IApplicationService extends IService<Application, Long>
{
    public Long getApplicationQty() throws ImperiumException;

    public List<Application> findAll(Integer page, Integer maxSize,
        String sortCriteria) throws ImperiumException;

    public List<Application> findAll(Map<String, Object> pagination,
        Map<String, Object> order) throws ImperiumException;

    public List<Application> findAllWithDetail(Integer page, Integer maxSize,
        String sortCriteria) throws ImperiumException;

    public Long getQtyForFindAllWhere(Map<String, Object> queryParams)
        throws ImperiumException;

    public List<Application> findAllWhere(Map<String, Object> queryParams,
        Map<String, Object> pagination, Map<String, Object> order)
        throws ImperiumException;

    public Application findById(Long id) throws ImperiumException;

    public Application findOneWithDetailById(Long id) throws ImperiumException;

    public Application create(Application application) throws ImperiumException;

    public Application update(Application application) throws ImperiumException;

    public void delete(Application application) throws ImperiumException;

    public void removePermissions(Application application,
        List<Permission> permissionList) throws ImperiumException;

    public void addPermissions(Application application,
        List<Permission> permissionList) throws ImperiumException;

    public void addRoles(Application application, List<Role> roleList)
        throws Exception;

    public void addSubjects(Application application, List<Subject> subjectList)
        throws Exception;

    public Map<String, Object> updateRoles(Long applicatioId, List<Long> toAdd,
        List<Long> toRemove) throws Exception;

    public void resetApiKey(Long applicationId) throws Exception;

}