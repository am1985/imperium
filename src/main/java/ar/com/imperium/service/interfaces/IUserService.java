/**
 * 
 */
package ar.com.imperium.service.interfaces;

import java.util.List;
import java.util.Map;

import ar.com.imperium.domain.User;

public interface IUserService extends IService<User, Long>
{
    public User findOneByUserAndPassword(String user, String password)
        throws Exception;

    public void changePassword(User user, String newPassword) throws Exception;

    public void changePassword(User user, String oldPassword, String newPassword)
        throws Exception;

    public List<User> listAll(Map<String, Object> pagination,
        Map<String, Object> order) throws Exception;

    public List<User> listAllWhere(Map<String, Object> queryParams,
        Map<String, Object> pagination, Map<String, Object> order)
        throws Exception;

    public Long getTotal() throws Exception;

    public Long getTotalWhere(Map<String, Object> queryParams) throws Exception;

}
