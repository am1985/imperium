package ar.com.imperium.dbutils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.context.support.GenericApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import ar.com.imperium.domain.Application;
import ar.com.imperium.domain.Permission;
import ar.com.imperium.domain.Role;
import ar.com.imperium.domain.Subject;
import ar.com.imperium.service.interfaces.IApplicationService;
import ar.com.imperium.service.interfaces.IRoleService;

public class NGNCoreSampleDbCreator {

	public static void main(String[] args) {
		try {
			NGNCoreSampleDbCreator create = new NGNCoreSampleDbCreator();
			create.create();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Application createApplication() throws Exception {
		Application application = new Application();
		application.setName("NGNCore1");
		application
				.setDescription("NGNCore a soft phone telephony application");
		application.setApiKey("key");

		List<Subject> subjectList = this.createSubjects();
		for (Subject eachSubject : subjectList) {
			application.addSubject(eachSubject);
		}

		return application;
	}

	private List<Subject> createSubjects() throws Exception {
		List<Subject> answer = new ArrayList<Subject>();
		Subject subject = this.createSubject();
		answer.add(subject);
		return answer;
	}

	private Subject createSubject() throws Exception {
		Subject answer = null;

		answer = new Subject("NGNCoreSubject");

		Role userRole = this.createUserRole();
		answer.addRole(userRole);

		return answer;
	}

	private List<Permission> createUserPermissions() {
		List<Permission> answer = new ArrayList<Permission>();

		String[] resources = new String[] { "language" };
		String[] actions = new String[] { "create", "read", "update", "delete" };

		Permission eachPermission;
		for (String eachAction : actions) {
			eachPermission = new Permission(resources[0], eachAction);
			answer.add(eachPermission);
		}
		return answer;
	}

	private Role createUserRole() throws Exception {
		Role userRole = new Role("user", "User role for ngnCore");

		List<Permission> permissionList = this.createUserPermissions();
		for (Permission eachPermission : permissionList) {
			userRole.addPermission(eachPermission);
		}

		return userRole;
	}

	private GenericApplicationContext bootstrapContext() {
		GenericXmlApplicationContext context = new GenericXmlApplicationContext();
		// context.load("classpath:jpa-app-context.xml");
		context.load("classpath:jpa-app-context.xml");
		context.refresh();
		return context;
	}

	public void create() throws Exception {
		GenericXmlApplicationContext context = (GenericXmlApplicationContext) bootstrapContext();
		IApplicationService service = context.getBean(
				"jpaComplexApplicationService", IApplicationService.class);

		IRoleService roleService = context.getBean("jpaComplexRoleService",
				IRoleService.class);
		
		Application ngnCore = this.createApplication();
		/*
		Set<Role> roles = ngnCore.getRoles();
		Iterator<Role> roleIterator = roles.iterator();
		Role currentRole = null;
		while(roleIterator.hasNext()) {
			currentRole = roleIterator.next();
			roleService.create(currentRole);
		}
		
		service.create(ngnCore);
		*/
		
		
	}
}
