package ar.com.imperium.dbutils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.aspectj.weaver.patterns.ThisOrTargetAnnotationPointcut;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import ar.com.imperium.domain.Application;
import ar.com.imperium.domain.Permission;
import ar.com.imperium.domain.Role;
import ar.com.imperium.domain.Subject;
import ar.com.imperium.domain.User;
import ar.com.imperium.domain.UserType;
import ar.com.imperium.service.interfaces.IApplicationService;
import ar.com.imperium.service.interfaces.IPermissionService;
import ar.com.imperium.service.interfaces.IRoleService;
import ar.com.imperium.service.interfaces.ISubjectService;
import ar.com.imperium.service.interfaces.IUserService;

public class NGNCoreSampleDbCreator2 {

	private GenericApplicationContext bootstrapContext() {
		GenericXmlApplicationContext context = new GenericXmlApplicationContext();
		// context.load("classpath:jpa-app-context.xml");
		context.load("classpath:jpa-app-context.xml");
		context.refresh();
		return context;
	}

	private Application createApplication() throws Exception {
		Application app = new Application();
		app.setName("NGNCore");
		app.setDescription("Description");
		app.setApiKey("key");
		GenericXmlApplicationContext context = (GenericXmlApplicationContext) bootstrapContext();
		IApplicationService service = context.getBean(
				"jpaComplexApplicationService", IApplicationService.class);

		app = service.create(app);

		System.out.println(app.getId());

		return app;
	}

	private List<Role> createRoles() throws Exception {
		GenericApplicationContext context = this.bootstrapContext();
		IRoleService roleService = (IRoleService) context
				.getBean("jpaComplexRoleService");

		List<Role> roles = new ArrayList<Role>();
		String[] roleNames = new String[] { "admin", "root", "user" };

		Role eachRole;
		for (String eachName : roleNames) {
			eachRole = new Role(eachName, eachName);
			roles.add(eachRole);
		}

		roles = roleService.create(roles);
		return roles;
	}

	private List<Subject> createSubjects() throws Exception {
		List<Subject> subjects = new ArrayList<Subject>();

		GenericApplicationContext context = this.bootstrapContext();

		ISubjectService subjectService = (ISubjectService) context
				.getBean("jpaComplexSubjectService");

		String[] names = new String[] { "admin", "root", "user" };

		Subject eachSubject;
		for (String eachName : names) {
			eachSubject = new Subject(eachName);
			subjects.add(eachSubject);
		}

		return subjectService.create(subjects);
	}

	private List<Permission> createPermissions() throws Exception {
		List<Permission> permissions = new ArrayList<Permission>();

		GenericApplicationContext context = this.bootstrapContext();
		IPermissionService permissionService = (IPermissionService) context
				.getBean("jpaComplexPermissionService");

		String[] actions = new String[] { "create", "read", "update", "delete" };

		String[] resources = new String[] { "user", "subscriber", "plan",
				"currency", "tax", "campaign", "trunk", "tariff",
				"preferences", "language" };

		Permission eachPermission;
		for (String eachResource : resources) {
			for (String eachAction : actions) {
				eachPermission = new Permission(eachResource, eachAction);
				permissions.add(eachPermission);
			}
		}

		return permissionService.create(permissions);
	}

	public void create() throws Exception {
		GenericApplicationContext context = this.bootstrapContext();
		IApplicationService service = context.getBean(
				"jpaComplexApplicationService", IApplicationService.class);
		IPermissionService permissionService = (IPermissionService) context
				.getBean("jpaComplexPermissionService");

		Application app = this.createApplication();

		List<Role> roles = this.createRoles();

		List<Permission> permissions = this.createPermissions();

		List<Subject> subjects = this.createSubjects();

		// add roles to the application
		service.addRoles(app, roles);

		// add permissions to the application
		service.addPermissions(app, permissions);

		// add subjects to ngn application
		service.addSubjects(app, subjects);

		// add to role root all the permissions
		app = service.findOneWithDetailById(app.getId());
		permissions = permissionService.findAllForApplication(app, 0, 1000,
				"name");
		Role rootRole = app.getRoleWithName("root");

		this.setupRootPermissions(rootRole, permissions, context);

		Role adminRole = app.getRoleWithName("admin");
		this.setupAdminPermissions(app, context);

		Role userRole = app.getRoleWithName("user");
		this.setupUserPermissions(app, context);

		this.setupSubjects(app, context);
	}

	private void setupSubjects(Application application,
			GenericApplicationContext context) throws Exception {
		IRoleService roleService = (IRoleService) context
				.getBean("jpaComplexRoleService");

		IPermissionService permissionService = (IPermissionService) context
				.getBean("jpaComplexPermissionService");

		Role userRole = application.getRoleWithName("user");
		Role adminRole = application.getRoleWithName("admin");
		Role rootRole = application.getRoleWithName("root");

		Subject userSubject = application.getSubjectWithName("user");
		Subject adminSubject = application.getSubjectWithName("admin");
		Subject rootSubject = application.getSubjectWithName("root");

		List<Subject> subjectList = new ArrayList<Subject>();
		
		subjectList.clear();
		subjectList.add(userSubject);
		roleService.addToRole(userRole, subjectList);
		
		subjectList.clear();
		subjectList.add(adminSubject);
		roleService.addToRole(adminRole, subjectList);
		
		subjectList.clear();
		subjectList.add(rootSubject);
		roleService.addToRole(rootRole, subjectList);
		
		this.createUsers((GenericXmlApplicationContext) context);
	}

	private void createUsers(GenericXmlApplicationContext context) throws Exception
	{
		User admin = new User("admin", "password", UserType.ADMIN.getType());
		User user = new User("user", "password", UserType.USER.getType());
		
		 IUserService service =
		            context.getBean("jpaUserService", IUserService.class);
		 
		 List<User> userList = new ArrayList<User>();
		 userList.add(admin);
		 userList.add(user);
		 
		 service.create(userList);
	}
	
	private void setupUserPermissions(Application application,
			GenericApplicationContext context) throws Exception {
		IRoleService roleService = (IRoleService) context
				.getBean("jpaComplexRoleService");

		List<Permission> eachPermissions = null;
		String[] resourceNames = new String[] { "preferences", "language" };

		Role adminRole = application.getRoleWithName("user");
		for (String eachName : resourceNames) {
			eachPermissions = application.getPermissionsForResource(eachName);
			roleService.assignPermissions(adminRole, eachPermissions);
		}

	}

	private void setupAdminPermissions(Application application,
			GenericApplicationContext context) throws Exception {
		IRoleService roleService = (IRoleService) context
				.getBean("jpaComplexRoleService");

		List<Permission> eachPermissions = null;
		String[] resourceNames = new String[] { "user", "preferences",
				"language" };

		Role adminRole = application.getRoleWithName("admin");
		for (String eachName : resourceNames) {
			eachPermissions = application.getPermissionsForResource(eachName);
			roleService.assignPermissions(adminRole, eachPermissions);
		}

	}

	private void setupRootPermissions(Role rootRole,
			List<Permission> permissions, GenericApplicationContext context)
			throws Exception {
		IRoleService roleService = (IRoleService) context
				.getBean("jpaComplexRoleService");

		roleService.assignPermissions(rootRole, permissions);
	}

	public static void main(String[] args) {
		try {
			NGNCoreSampleDbCreator2 creator2 = new NGNCoreSampleDbCreator2();
			creator2.create();
			System.out.println("Created the application");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
