/**
 * 
 */
package ar.com.imperium.repository;

import java.util.List;
import java.util.Map;

import ar.com.imperium.domain.Application;
import ar.com.imperium.domain.Permission;
import ar.com.imperium.domain.Role;
import ar.com.imperium.domain.Subject;
import ar.com.imperium.exception.ImperiumException;

public interface IApplicationRepository extends
    IBaseRepository<Application, Long>
{
    public void addPermissions(Application application,
        List<Permission> permissionList) throws ImperiumException;

    public void addRoles(Application application, List<Role> roleList)
        throws Exception;

    public void addSubjects(Application application, List<Subject> subjectList)
        throws Exception;

    public List<Application> findAllWhere(Map<String, Object> queryParams,
        Map<String, Object> pagination, Map<String, Object> order)
        throws ImperiumException;

    public Long getQtyForFindAllWhere(Map<String, Object> queryParams)
        throws ImperiumException;

    public Map<String, Object> updateRoles(Long applicatioId, List<Long> toAdd,
        List<Long> toRemove) throws ImperiumException;
}
