/**
 * 
 */
package ar.com.imperium.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import ar.com.imperium.domain.Application;
import ar.com.imperium.exception.ImperiumException;

/**
 * @author user
 * 
 */
public interface IBaseRepository<T, ID extends Serializable>
{
    public void setClass(final Class<T> tClass);

    public T findOneById(ID id);

    public T create(T t);

    public T update(T t);

    public T update(ID id, Map<String, Object> newValues);

    public void delete(T t);

    public List<Application> findAll(Map<String, Object> pagination,
        Map<String, Object> order) throws ImperiumException;

    public List<T> findAll(Integer page, Integer pageSize, String sortCriteria);

    public List<T> create(List<T> entityList);

    public List<T> findWithIds(List<ID> idList);

    public List<T> findAllForApplicationWhere(Application application,
        Map<String, Object> queryParams, Map<String, Object> pagination,
        Map<String, Object> order);

    public Long findQtyForApplicationWhere(Application application,
        Map<String, Object> queryParams);
    
    public void remove(List<T> entityList);

}