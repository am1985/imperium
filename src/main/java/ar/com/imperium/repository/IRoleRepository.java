/**
 * 
 */
package ar.com.imperium.repository;

import java.util.List;
import java.util.Map;

import ar.com.imperium.domain.Application;
import ar.com.imperium.domain.Permission;
import ar.com.imperium.domain.Role;
import ar.com.imperium.domain.Subject;
import ar.com.imperium.exception.ImperiumException;

/**
 * @author user
 * 
 */
public interface IRoleRepository extends IBaseRepository<Role, Long>
{
    public List<Subject> addToRole(Role role, List<Subject> subjectList)
        throws ImperiumException;

    public Long findQtyForApplication(Application application)
        throws ImperiumException;

    public List<Role> findAllForApplication(Application application,
        Integer page, Integer maxSize, String sortCriteria)
        throws ImperiumException;

    public List<Permission> findPermissions(Role role,
        Map<String, Object> pagination);

    public List<Permission> findPermissions(Role role,
        Map<String, Object> pagination, Map<String, Object> order);

    public Long findQtyForPermissionsForRole(Map<String, Object> queryParams);

    public List<Permission> findPermissionForRole(
        Map<String, Object> queryParams, Map<String, Object> pagination,
        Map<String, Object> order);

    public Long findPermissionQty(Role role);

    public void assignPermissions(Role role, List<Permission> permissionList)
        throws ImperiumException;

    public void removePermissions(Role role, List<Permission> permissionList)
        throws ImperiumException;

    public List<Role> findForSubjectInApplication(
        Map<String, Object> queryParams, Map<String, Object> pagination,
        Map<String, Object> order) throws Exception;

    public Long findQtyForSubjectInApplication(Map<String, Object> queryParams)
        throws Exception;

    public List<Role> findAvailableForSubjectInApplication(
        Map<String, Object> queryParams, Map<String, Object> pagination,
        Map<String, Object> order) throws Exception;

    public Long findAvailableQtyForSubjectInApplication(
        Map<String, Object> queryParams) throws Exception;

}