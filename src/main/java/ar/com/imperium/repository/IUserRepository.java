/**
 * 
 */
package ar.com.imperium.repository;

import java.util.List;
import java.util.Map;

import ar.com.imperium.domain.User;

public interface IUserRepository extends IBaseRepository<User, Long>
{
    public User getByUserAndPassword(String user, String password)
        throws Exception;

    public void changePassword(User user, String newPassword) throws Exception;

    public List<User> listAll(Map<String, Object> pagination,
        Map<String, Object> order) throws Exception;

    public List<User> listAllWhere(Map<String, Object> queryParams,
        Map<String, Object> pagination, Map<String, Object> order)
        throws Exception;

    public Long getTotal() throws Exception;

    public Long getTotalWhere(Map<String, Object> queryParams) throws Exception;
}
