/**
 * 
 */
package ar.com.imperium.exception;

/**
 * @author user
 * 
 */
public class EntityNotFoundException extends ImperiumException
{
    private static final long serialVersionUID = 5395941555278609687L;
    protected String attributeName;
    protected Object value;

    protected void subConstructor(String attributeName, Object value)
    {
        this.attributeName = attributeName;
        this.value = value;
    }

    public EntityNotFoundException(String className, String attributeName,
        Object value)
    {
        super("Cannot find object of class " + className
            + " with using attribute " + attributeName + " with value " + value);

        subConstructor(attributeName, value);
    }

    public EntityNotFoundException(String attributeName, Object value)
    {
        super("Cannot find entity");
        subConstructor(attributeName, value);
    }

    public EntityNotFoundException(String attributeName, Object value,
        boolean ajaxResponse)
    {
        super("Cannot find entity with attribute :" + attributeName + ":"
            + value, ajaxResponse);

        subConstructor(attributeName, value);
    }

    public String getAttributeName()
    {
        return attributeName;
    }

    public Object getValue()
    {
        return value;
    }
}
