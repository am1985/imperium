/**
 * 
 */
package ar.com.imperium.exception;

public class ImperiumRuntimeException extends RuntimeException
{
    private static final long serialVersionUID = -2555505687451397596L;

    public ImperiumRuntimeException(String message)
    {
        super(message);
    }
}
