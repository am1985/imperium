package ar.com.imperium.exception;

public class NotLoggedInException extends ImperiumRuntimeException
{

    public NotLoggedInException(String message)
    {
        super(message);
    }

}
