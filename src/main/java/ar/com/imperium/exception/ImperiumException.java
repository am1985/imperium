/**
 * 
 */
package ar.com.imperium.exception;

import java.util.Map;

/**
 * @author user
 * 
 */
public class ImperiumException extends Exception
{
	protected Map<String, String> errorMap;

    protected boolean ajaxResponse = false;

    /**
     * 
     */
    private static final long serialVersionUID = -5564753084151310517L;

    public ImperiumException(String message)
    {
        super(message);
        this.ajaxResponse = false;
    }

    public ImperiumException(String message, boolean ajaxResponse)
    {
        super(message);
        this.ajaxResponse = ajaxResponse;
    }
    
    public ImperiumException(String message, boolean ajaxResponse, Map<String, String> errorMap) 
    {
    	super(message);
    	this.ajaxResponse = ajaxResponse;
    	this.errorMap = errorMap;
    }

    public void setAsAjaxResponse()
    {
        this.ajaxResponse = true;
    }

    public boolean isAjaxResponse()
    {
        return ajaxResponse;
    }

    public boolean hasErrorMap()
    {
    	return (errorMap!=null);
    }
    
    public Map<String, String> getErrorMap() 
    {
    	return errorMap;
    }
    
}
