/**
 * 
 */
package ar.com.imperium.exception;

import ar.com.imperium.domain.Application;
import ar.com.imperium.domain.Role;

public class InvalidRoleAdditionToApplication extends ImperiumRuntimeException
{
    private static final long serialVersionUID = 5813322680429943032L;
    protected Application application;
    protected Role role;

    public InvalidRoleAdditionToApplication(Application application, Role role)
    {
        super("Cannot add the role with id " + role.getId()
            + " to the application " + application.getId());
        this.application = application;
        this.role = role;

    }
}
