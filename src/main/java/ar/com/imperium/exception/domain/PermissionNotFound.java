/**
 * 
 */
package ar.com.imperium.exception.domain;

import ar.com.imperium.exception.EntityNotFoundException;

/**
 * @author user
 * 
 */
@SuppressWarnings("serial")
public class PermissionNotFound extends EntityNotFoundException
{
    public PermissionNotFound(String attributeName, Object value)
    {
        super("Permission", attributeName, value);
    }

    public PermissionNotFound(String attributeName, Object value,
        boolean ajaxResponse)
    {
        super("Permission", attributeName, value);
        if (ajaxResponse) {
            setAsAjaxResponse();
        }
    }
}
