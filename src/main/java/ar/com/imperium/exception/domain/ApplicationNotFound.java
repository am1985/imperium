/**
 * 
 */
package ar.com.imperium.exception.domain;

import ar.com.imperium.exception.EntityNotFoundException;

/**
 * @author user
 * 
 */
@SuppressWarnings("serial")
public class ApplicationNotFound extends EntityNotFoundException
{

    public ApplicationNotFound(String attributeName, Object value)
    {
        super("Application", attributeName, value);
    }

    public ApplicationNotFound(String attributeName, Object value,
        boolean ajaxResponse)
    {
        super("Application", attributeName, value);
        if (ajaxResponse) {
            setAsAjaxResponse();
        }
    }
}
