/**
 * 
 */
package ar.com.imperium.exception.domain;

import ar.com.imperium.exception.EntityNotFoundException;

/**
 * @author user
 * 
 */
public class RoleNotFound extends EntityNotFoundException
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public RoleNotFound(String attributeName, Object value)
    {
        super("Role", attributeName, value);
    }

    public RoleNotFound(String attributeName, Object value, boolean ajaxResponse)
    {
        super("Role", attributeName, value);
        if (ajaxResponse) {
            setAsAjaxResponse();
        }
    }

}
