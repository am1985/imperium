package ar.com.imperium.exception.domain;

import ar.com.imperium.domain.Application;
import ar.com.imperium.domain.Permission;
import ar.com.imperium.exception.ImperiumException;

/**
 * @author user
 * 
 */
public class PermissionNotFoundInApplication extends ImperiumException
{
    private Application application;
    private Permission permission;

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * @param message
     */
    public PermissionNotFoundInApplication(String message)
    {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public PermissionNotFoundInApplication()
    {
        super("");
    }

    public PermissionNotFoundInApplication(String message,
        Application application, Permission permission)
    {
        super(message);
        this.application = application;
        this.permission = permission;
    }

    public Application getApplication()
    {
        return application;
    }

    public void setApplication(Application application)
    {
        this.application = application;
    }

    public Permission getPermission()
    {
        return permission;
    }

    public void setPermission(Permission permission)
    {
        this.permission = permission;
    }

}
