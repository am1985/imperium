/**
 * 
 */
package ar.com.imperium.exception.domain;

import ar.com.imperium.exception.EntityNotFoundException;

/**
 * @author user
 * 
 */
public class SubjectNotFound extends EntityNotFoundException
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public SubjectNotFound(String attributeName, Object value)
    {
        super("Subject", attributeName, value);
    }

    public SubjectNotFound(String attributeName, Object value,
        boolean ajaxResponse)
    {
        super("Subject", attributeName, value);
        if (ajaxResponse) {
            setAsAjaxResponse();
        }
    }

}
