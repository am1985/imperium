/**
 * 
 */
package ar.com.imperium.exception.domain;

import ar.com.imperium.exception.EntityNotFoundException;

public class UserNotFound extends EntityNotFoundException
{

    public UserNotFound(String attributeName, Object value)
    {
        super("User", attributeName, value);
    }

    public UserNotFound(String attributeName, Object value, boolean ajaxResponse)
    {
        super("User", attributeName, value);
        if (ajaxResponse) {
            setAsAjaxResponse();
        }
    }

}
