/**
 * 
 */
package ar.com.imperium.common;

/**
 * @author user
 * 
 */
public enum PrimitiveTypes
{
    STRING_TYPE("String"), INTEGER_TYPE("Integer"), FLOAT_TYPE("Float");

    private String name;

    private PrimitiveTypes(String name)
    {
        this.name = name;
    }

}
