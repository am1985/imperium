/**
 * 
 */
package ar.com.imperium.common;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component("arrayHelper")
public class ArrayHelper
{
    public List<Long> convertToLongList(List<String> stringList)
        throws Exception
    {
        List<Long> answer = new ArrayList<Long>();
        for (String eachString : stringList) {
            answer.add(Long.parseLong(eachString));
        }
        return answer;
    }
}
