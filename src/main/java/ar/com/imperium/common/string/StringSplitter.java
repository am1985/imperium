package ar.com.imperium.common.string;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Splitter;

import org.springframework.stereotype.Component;

@Component("stringSplitter")
public class StringSplitter {

	private Iterable<String> subSplit(String input, String separator) {
		return Splitter.on(separator).trimResults().omitEmptyStrings().split(input);
	}
	
	public Iterable<String> split(String input) {
		return subSplit(input, ",");
	}
	
	public List<String> getListFromSplit(String input, String separator) 
	{
		List<String> answer = new ArrayList<String>();
		Iterable<String> stringIterable = subSplit(input, separator);
		for (String string : stringIterable) {
			answer.add(string);
		}
		return answer;
	}
}
