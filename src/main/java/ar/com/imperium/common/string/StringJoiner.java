package ar.com.imperium.common.string;

import java.util.List;
import org.springframework.stereotype.Component;
import com.google.common.base.Joiner;

@Component("stringJoiner")
public class StringJoiner {

	public String join(List<String> string, String separator) {
		Joiner joiner = Joiner.on(separator).skipNulls();
		return joiner.join(string);
	}

}
