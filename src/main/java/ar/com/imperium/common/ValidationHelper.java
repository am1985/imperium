package ar.com.imperium.common;

import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component("validationHelper")
public class ValidationHelper {

	private Pattern wordPattern;
	private Pattern wordListPattern;

	public ValidationHelper() {
		wordListPattern = Pattern.compile("^[a-zA-Z0-9]+(,[a-zA-Z0-9]+)*$");
		wordPattern = Pattern.compile("^[a-zA-Z0-9]+$");
	}

	public boolean isValidWord(String word) {
		return wordPattern.matcher(word).matches();
	}

	public boolean isValidWordList(String input) {
		return (wordListPattern.matcher(input).matches());
	}

}
