/**
 * 
 */
package ar.com.imperium.common;

import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Component("springHelper")
public class SpringFrameworkHelper
{

    /**
     * Add the values from the map to the request for a flash redirect
     * 
     * @param values
     * @param redirectAttributes
     */
    public void addValuesForFlashRedirect(Map<String, Object> values,
        RedirectAttributes redirectAttributes)
    {

        Set<String> keys = values.keySet();
        for (String eachKey : keys) {
            redirectAttributes.addFlashAttribute(eachKey, values.get(eachKey));
        }
    }

}
