/**
 * 
 */
package ar.com.imperium.common.json.gson.entitiyserializers;

import java.lang.reflect.Type;

import ar.com.imperium.domain.Application;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * @author user
 * 
 */
public class ApplicationSerializer implements JsonSerializer<Application>
{

    @Override
    public JsonElement serialize(Application src, Type typeOfSrc,
        JsonSerializationContext context)
    {
        JsonElement answer;

        if (src == null) {
            answer = JsonNull.INSTANCE;
        } else {
            JsonObject tempObject = new JsonObject();
            tempObject.add("id", new JsonPrimitive(src.getId()));
            tempObject.add("name", new JsonPrimitive(src.getName()));
            tempObject.add(
                "description",
                new JsonPrimitive(src.getDescription()));
            answer = tempObject;
        }
        return answer;
    }
}
