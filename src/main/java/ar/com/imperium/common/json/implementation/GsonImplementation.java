/**
 * 
 */
package ar.com.imperium.common.json.implementation;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import ar.com.imperium.common.json.IJsonHelper;
import ar.com.imperium.common.json.gson.entitiyserializers.ApplicationSerializer;
import ar.com.imperium.common.json.gson.entitiyserializers.PermissionSerializer;
import ar.com.imperium.common.json.gson.entitiyserializers.RoleSerializer;
import ar.com.imperium.common.json.gson.entitiyserializers.SubjectSerializer;
import ar.com.imperium.domain.Application;
import ar.com.imperium.domain.Permission;
import ar.com.imperium.domain.Role;
import ar.com.imperium.domain.Subject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * @author user
 * 
 */
@Component("gsonJsonHelper")
public class GsonImplementation implements IJsonHelper
{
    private Gson gson;

    public GsonImplementation()
    {
        subConstructor();
    }

    private void subConstructor()
    {
        GsonBuilder gsonBuilder = new GsonBuilder();

        ApplicationSerializer appSerializer = new ApplicationSerializer();

        gsonBuilder.setPrettyPrinting();
        gsonBuilder.registerTypeAdapter(Application.class, appSerializer);
        gsonBuilder.registerTypeAdapter(
            Permission.class,
            new PermissionSerializer());
        gsonBuilder.registerTypeAdapter(Subject.class, new SubjectSerializer());
        gsonBuilder.registerTypeAdapter(Role.class, new RoleSerializer());

        this.gson = gsonBuilder.create();
    }

    @Override
    public String toJson(Object src) throws Exception
    {
        return gson.toJson(src);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.imperium.common.json.IJsonHelper#getServerAnswerTempalte(boolean)
     */
    @Override
    public String getServerAnswerTemplate(boolean success) throws Exception
    {
        Map<String, Object> aMap = new HashMap<String, Object>();
        aMap.put("success", success);
        aMap.put("errorCode", "{toReplaceErrorCode}");
        aMap.put("data", "{toReplaceData}");
        aMap.put("errorCode", "{toReplaceErrorCode}");
        aMap.put("message", "{toReplaceErrorMessage}");
        aMap.put("total", "{toReplaceTotal}");

        return gson.toJson(aMap, aMap.getClass());
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.imperium.common.json.IJsonHelper#decodeJson(java.lang.String)
     */
    @Override
    public Map<String, Object> decodeJson(String content) throws Exception
    {
        Map<String, Object> answer = gson.fromJson(content, Map.class);
        return answer;
    }

    public String encodeMap(Map map) throws Exception
    {
        return gson.toJson(map, Map.class);
    }

}
