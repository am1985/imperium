/**
 * 
 */
package ar.com.imperium.common.json.gson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ar.com.imperium.domain.Application;

import com.google.gson.JsonElement;

/**
 * @author user
 * 
 */
public class ApplicationTypeAdapterFactory extends
    CustomizedTypeAdapterFactory<Application>
{
    public static Logger logger = LoggerFactory
        .getLogger(ApplicationTypeAdapterFactory.class);

    public ApplicationTypeAdapterFactory()
    {
        super(Application.class);
    }

    @Override
    protected void beforeWrite(Application source, JsonElement toSerialize)
    {

    }

    @Override
    protected void afterRead(JsonElement deserialized)
    {
    }

}
