/**
 * 
 */
package ar.com.imperium.common.json;

import java.util.Map;

/**
 * @author user
 * 
 */
public interface IJsonHelper
{
    public String toJson(Object src) throws Exception;

    public String getServerAnswerTemplate(boolean success) throws Exception;

    public Map<String, Object> decodeJson(String content) throws Exception;

    public String encodeMap(Map map) throws Exception;
}
