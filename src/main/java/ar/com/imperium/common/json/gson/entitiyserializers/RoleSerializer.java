/**
 * 
 */
package ar.com.imperium.common.json.gson.entitiyserializers;

import java.lang.reflect.Type;

import ar.com.imperium.domain.Role;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * @author user
 * 
 */
public class RoleSerializer implements JsonSerializer<Role>
{

    @Override
    public JsonElement serialize(Role src, Type typeOfSrc,
        JsonSerializationContext context)
    {
        JsonElement answer;

        if (src == null) {
            answer = JsonNull.INSTANCE;
        } else {
            JsonObject tempObject = new JsonObject();
            tempObject.add("id", new JsonPrimitive(src.getId()));
            tempObject.add("name", new JsonPrimitive(src.getName()));
            tempObject.add("DT_RowId", new JsonPrimitive(src.getId()));
            tempObject.add(
                "description",
                new JsonPrimitive(src.getDescription()));

            String appString = null;
            if (src.hasApplication()) {
                appString = src.getApplication().getName();
            } else {
                appString = "NULL";
            }
            tempObject.add("application", new JsonPrimitive(appString));
            answer = tempObject;
        }
        return answer;
    }

}
