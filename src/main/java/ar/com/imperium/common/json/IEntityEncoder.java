/**
 * 
 */
package ar.com.imperium.common.json;

/**
 * @author user
 * 
 */
public interface IEntityEncoder
{
    public String encode(Object object) throws Exception;
}
