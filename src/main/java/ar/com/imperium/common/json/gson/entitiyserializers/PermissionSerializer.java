/**
 * 
 */
package ar.com.imperium.common.json.gson.entitiyserializers;

import java.lang.reflect.Type;

import ar.com.imperium.domain.Permission;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * @author user
 * 
 */
public class PermissionSerializer implements JsonSerializer<Permission>
{

    /*
     * (non-Javadoc)
     * 
     * @see com.google.gson.JsonSerializer#serialize(java.lang.Object,
     * java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
     */
    @Override
    public JsonElement serialize(Permission src, Type typeOfSrc,
        JsonSerializationContext context)
    {
        JsonElement answer;

        if (src == null) {
            answer = JsonNull.INSTANCE;
        } else {
            JsonObject tempObject = new JsonObject();
            tempObject.add("id", new JsonPrimitive(src.getId()));
            tempObject.add("resource", new JsonPrimitive(src.getResource()));
            tempObject.add("DT_RowId", new JsonPrimitive(src.getId()));
            tempObject.add("action", new JsonPrimitive(src.getAction()));
            tempObject.add(
                "description",
                new JsonPrimitive(src.getDescriptionString()));

            String appString = null;
            if (src.hasApplication()) {
                appString = src.getApplication().getName();
            } else {
                appString = "NULL";
            }
            tempObject.add("application", new JsonPrimitive(appString));
            answer = tempObject;
        }
        return answer;
    }

}
