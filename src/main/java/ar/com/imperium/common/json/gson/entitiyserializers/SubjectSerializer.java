/**
 * 
 */
package ar.com.imperium.common.json.gson.entitiyserializers;

import java.lang.reflect.Type;

import ar.com.imperium.domain.Subject;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * @author user
 * 
 */
public class SubjectSerializer implements JsonSerializer<Subject>
{
    @Override
    public JsonElement serialize(Subject src, Type typeOfSrc,
        JsonSerializationContext context)
    {
        JsonElement answer = null;

        if (src == null) {
            answer = JsonNull.INSTANCE;
        } else {
            JsonObject tempObject = new JsonObject();
            tempObject.add("id", new JsonPrimitive(src.getId()));
            tempObject.add("DT_RowId", new JsonPrimitive(src.getId()));
            tempObject.add("name", new JsonPrimitive(src.getName()));

            String application = "";
            if (src.hasApplication()) {
                application = src.getApplication().getName();
            }
            tempObject.add("application", new JsonPrimitive(application));

            answer = tempObject;
        }
        return answer;
    }

}
