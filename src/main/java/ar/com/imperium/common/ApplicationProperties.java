/**
 * 
 */
package ar.com.imperium.common;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @todo add a method to create a map with the default pagination values
 * 
 * @author user
 * 
 */
@Component("applicationPropertiesHelper")
public class ApplicationProperties implements IPropertiesHelper
{
    @Autowired
    @Qualifier("applicationProperties")
    private Properties applicationProperties;

    private static final Logger logger = LoggerFactory
        .getLogger(ApplicationProperties.class);

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.imperium.common.IPropertiesHelper#getProperty(java.lang.String)
     */
    @Override
    public String getProperty(String name)
    {
        String value = applicationProperties.getProperty(name);
        return value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.imperium.common.IPropertiesHelper#contains(java.lang.String)
     */
    @Override
    public boolean contains(String propertyName)
    {
        return (applicationProperties.getProperty(propertyName) != null);
    }

}
