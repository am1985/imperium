package ar.com.imperium.common.helper;

import org.apache.log4j.Logger;

public class LoggingHelper {

	private static final Logger logger = Logger.getLogger(LoggingHelper.class);
	
	public static void log(String message) 
	{
		logger.error(message);
	}
	
}
