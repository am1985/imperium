/**
 * 
 */
package ar.com.imperium.common.security;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ar.com.imperium.web.app.controller.SecurityController;

public class SecurityFilter implements Filter
{
    private static final Logger logger = LoggerFactory
        .getLogger(SecurityFilter.class);

    public SecurityFilter()
    {
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
        FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        HttpSession session = req.getSession(false);

        Map<String, Object> userInfo =
            (Map<String, Object>) ((session != null) ? (session
                .getAttribute(SecurityController.PARAM_NAME_USER)) : null);
        boolean isAuthenticated = (userInfo != null);

        if (isAuthenticated) {
            chain.doFilter(request, response);
        } else {
            res.sendError(401);
            return;
        }
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException
    {
    }

}
