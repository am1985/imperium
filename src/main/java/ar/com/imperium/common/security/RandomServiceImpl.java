/**
 * 
 */
package ar.com.imperium.common.security;

import java.security.SecureRandom;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

@Component("randomService")
public class RandomServiceImpl implements IRandomService
{
    private SecureRandom secureRandom;

    public RandomServiceImpl() throws Exception
    {
        secureRandom = SecureRandom.getInstance("SHA1PRNG");
    }

    @Override
    public String generateRandomString(int size) throws Exception
    {
        String randomString = new Integer(secureRandom.nextInt()).toString();
        String hash = DigestUtils.sha512Hex(randomString);
        String answer = hash;
        if (answer.length() >= size) {
            answer = hash.substring(0, size - 1);
        }
        return answer;
    }

}
