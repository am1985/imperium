/**
 * 
 */
package ar.com.imperium.common.security;

public interface IRandomService
{
    public String generateRandomString(int size) throws Exception;
}
