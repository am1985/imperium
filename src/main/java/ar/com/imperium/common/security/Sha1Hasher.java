/**
 * 
 */
package ar.com.imperium.common.security;

import org.apache.commons.codec.digest.DigestUtils;

public class Sha1Hasher implements IHashService
{
    @Override
    public String hashString(String input) throws Exception
    {
        String hashed = DigestUtils.sha512Hex(input);
        String answer = hashed;
        if (hashed.length() >= (IHashService.HASH_LENGTH + 1)) {
            answer = hashed.substring(0, IHashService.HASH_LENGTH);
        }
        return answer;
    }

}
