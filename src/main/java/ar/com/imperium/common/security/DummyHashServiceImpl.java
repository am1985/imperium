/**
 * 
 */
package ar.com.imperium.common.security;

import org.springframework.stereotype.Component;

@Component("dummyHashService")
public class DummyHashServiceImpl implements IHashService
{

    @Override
    public String hashString(String input) throws Exception
    {
        return input;
    }

}
