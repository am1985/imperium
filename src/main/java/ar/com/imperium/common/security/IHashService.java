/**
 * 
 */
package ar.com.imperium.common.security;

public interface IHashService
{
    public static final int HASH_LENGTH = 256;

    public String hashString(String input) throws Exception;
}
