/**
 * 
 */
package ar.com.imperium.common;

/**
 * @author user
 * 
 */
public interface IPropertiesHelper
{
    public String getProperty(String name);

    public boolean contains(String propertyName);

}
