/**
 * 
 */
package ar.com.imperium.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * @author user
 * 
 */
public enum UserType
{
    USER(0, "USER"), ADMIN(1, "ADMIN");

    private Integer type;
    private String representation;

    private UserType(Integer type, String representation)
    {
        this.type = type;
        this.representation = representation;
    }

    public String toString()
    {
        return representation;
    }

    public Integer getType()
    {
        return this.type;
    }

    public Map<String, Object> getAsMap()
    {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("type", type);
        map.put("representation", representation);
        return map;
    }

    public static UserType fromCode(Integer type)
    {
        UserType answer = null;
        if (type == UserType.ADMIN.getType()) {
            answer = UserType.ADMIN;
        } else {
            answer = UserType.USER;
        }
        return answer;
    }

}
