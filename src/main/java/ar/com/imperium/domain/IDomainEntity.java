/**
 * 
 */
package ar.com.imperium.domain;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Transient;

/**
 * @author user
 * 
 */
public interface IDomainEntity<E> extends Comparable<E>, Serializable
{

    public Long getId();

    public int hashCode();

    public boolean equals(Object object);

    public String toString();

    @Transient
    public Map<String, Object> getAsMap() throws Exception;

}
