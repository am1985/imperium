var imperium_ui_dao_User = function() {

	var urlManager = imperium_ui_utils_UrlManager.module;

	var createCb = function(data, successCb, failureCb) {
		$.ajax({
			url : urlManager.getUrlForUserCreateSubmit(),
			cache : false,
			type : "POST",
			success : function(answerData, textStatus, xmlHttpRequest) {
				successCb(answerData);
			},
			failure : function(xmlHttpRequest, textStatus, errorThrown) {
				failureCb(xmlHttpRequest);
			},
			contentType : "application/json; charset=UTF-8",
			data : JSON.stringify(data)
		});
	};

	var editCb = function(data, successCb, failureCb) {
		$.ajax({
			url : urlManager.getUrlForUserEditSubmit(),
			cache : false,
			type : "POST",
			success : function(answerData, textStatus, xmlHttpRequest) {
				successCb(answerData);
			},
			failure : function(xmlHttpRequest, textStatus, errorThrown) {
				failureCb(xmlHttpRequest);
			},
			contentType : "application/json; charset=UTF-8",
			data : JSON.stringify(data)
		});

	};

	var removeCb = function(data, successCb, failureCb) {
		$.ajax({
			url : urlManager.getUrlForUserRemoval(),
			cache : false,
			type : "POST",
			success : function(answerData, textStatus, xmlHttpRequest) {
				successCb(answerData);
			},
			failure : function(xmlHttpRequest, textStatus, errorThrown) {
				failureCb(xmlHttpRequest);
			},
			contentType : "application/json; charset=UTF-8",
			data : JSON.stringify(data)
		});
	};

	return {
		create : createCb,
		edit : editCb,
		remove : removeCb
	};
}();