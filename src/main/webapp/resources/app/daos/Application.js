var imperium_ui_dao_Application = function() {

	var urlManager = imperium_ui_utils_UrlManager.module;

	var getApplicationCb = function(id, onSuccessCb, onFailureCb) {
		var url = urlManager.getUrlForAppShowInfo(id);
		url = url.replace("{id}", id);

		var aSuccessCb = function(data, textStatus, xmlHttpRequest) {
			onSuccessCb(data);
		};

		var aFailureCb = function(xmlHttpRequest, textStatus, errorThrown) {
			onFailureCb(xmlHttpRequest);
		};

		var options = {
			url : url,
			cache : false,
			type : "GET",
			success : aSuccessCb,
			failure : aFailureCb
		};

		$.ajax(options);
	};

	var editApplication = function(data, onSuccessCb, onFailureCb) {
		var aSuccessCb = function(data, textStatus, xmlHttpRequest) {
			onSuccessCb(data);
		};

		var aFailureCb = function(xmlHttpRequest, textStatus, errorThrown) {
			onFailureCb(xmlHttpRequest);
		};

		var options = {
			url : urlManager.getUrlForAppEditSubmit(),
			cache : false,
			type : "POST",
			success : aSuccessCb,
			failure : aFailureCb,
			contentType : "application/json; charset=UTF-8",
			data : JSON.stringify(data)
		};

		$.ajax(options);
	};

	var createApplicationCb = function(data, onSuccessCb, onFailureCb) {
		var url = urlManager.getUrlForAppCreateSubmit();

		var aSuccessCb = function(data, textStatus, xmlHttpRequest) {
			onSuccessCb(data);
		};

		var aFailureCb = function(xmlHttpRequest, textStatus, errorThrown) {
			onFailureCb(xmlHttpRequest);
		};

		var options = {
			url : url,
			cache : false,
			type : "POST",
			success : aSuccessCb,
			failure : aFailureCb,
			contentType : "application/json; charset=UTF-8",
			data : JSON.stringify(data)
		};

		$.ajax(options);
	};

	/**
	 * data js Object data["idsToRemove"]=[1,2,3,4....]
	 * data["applicationId"]=Application Id
	 */
	var removePermissionsCb = function(data, onSuccessCb, onFailureCb) {
		var url = "/imperium/permission/ajax/removal-from-app";

		var aSuccessCb = function(data, textStatus, xmlHttpRequest) {
			onSuccessCb(data);
		};

		var aFailureCb = function(xmlHttpRequest, textStatus, errorThrown) {
			onFailureCb(xmlHttpRequest);
		};

		var options = {
			url : url,
			cache : false,
			type : "POST",
			success : aSuccessCb,
			failure : aFailureCb,
			contentType : "application/json; charset=UTF-8",
			data : JSON.stringify(data)
		};

		$.ajax(options);
	};

	var resetKeyCb = function(id, onSuccessCb, onFailureCb) {
		var url = urlManager.getUrlForAppResetKey(id);

		var aSuccessCb = function(data, textStatus, xmlHttpRequest) {
			onSuccessCb(data);
		};

		var aFailureCb = function(xmlHttpRequest, textStatus, errorThrown) {
			onFailureCb(xmlHttpRequest);
		};

		var options = {
			url : url,
			cache : false,
			type : "GET",
			success : aSuccessCb,
			failure : aFailureCb,
		};

		$.ajax(options);

	};

	var removeCb = function(id, successCb, failureCb) {
		var url = urlManager.getUrlForApplicationRemoval(id);
		var options = {
			url:url,
			cache:false,
			success:successCb,
			failure:failureCb
		};
		
		$.ajax(options);
	};

	return {
		get : getApplicationCb,
		edit : editApplication,
		removePermissionsFromApp : removePermissionsCb,
		create : createApplicationCb,
		resetKey : resetKeyCb,
		remove:removeCb
	};

}();