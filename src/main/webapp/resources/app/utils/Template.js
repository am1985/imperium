var imperium_ui_utils_Template = function() {
	'use strict';
	
	var getPath = function(name) {
		var url="/app/templates/{name}.js";
		url = url.replace("{name}", name);
		return url;
	};
	
	var getTemplate = function(name, successCb) {
		
		var url = getPath(name);
		
		var options = {
			method:'GET',
			cache:true,
			success:function(data) {
				successCb(data);
			}
		};
		$.ajax(options);
	};
	
	return {
		getTemplate:getTemplate
	};
}();