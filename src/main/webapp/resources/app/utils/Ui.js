var imperium_ui_utils_Ui = function() {
	"use strict";
	
	var convertArrayToMapCb = function(anArray) {
		var answer = {};
		
		var eachName=null;
		var eachValue=null;
		var eachInput=null;
		var index=null;
		
		for (index in anArray) {
			eachInput = anArray[index];
			eachName = eachInput["name"];
			eachValue = eachInput["value"];
			
			answer[eachName] = eachValue;
		}
		
		return answer;
	};
	/**
	 * Apply the toString method to each element
	 * of an array
	 */
	var mapElementsToString=function(anArray) {
		var answer = [];
		var mapCb = function(index, element) {
			answer.push(element.toString());
		};
		$.each(anArray, mapCb);
		
		return answer;
	};
	
	/**
	 * Read the content of a handle bars template,
	 * compile it and apply with the data
	 */
	var generateContentFromTemplate= function(templateSelector, data) {
		var source = $(templateSelector).html();
		var templateCb = Handlebars.compile(source);
		return templateCb(data);
	};
	
	return {
		convertArrayToMap:convertArrayToMapCb,
		mapElementsToString:mapElementsToString,
		generateContentFromTemplate:generateContentFromTemplate
	};
}();
