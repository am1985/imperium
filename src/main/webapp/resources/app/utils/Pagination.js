var imperium_ui_utils_Pagination = function() {
	'use strict';
	var getPageQty = function(pageSize, total) {
		return (Math.ceil((total * 1.0) / (pageSize * 1.0)));
	};

	var calculatePagination = function(currentPage, pageSize, total) {
		var pageQty = getPageQty(pageSize, total);
		var beforePage = (currentPage >= 1) ? (currentPage - 1) : 0;
		var nextPage = ((currentPage + 1) >= pageQty) ? (pageQty - 1)
				: (currentPage + 1);

		var answer = {
			firstPage : 0,
			lastPage : (pageQty - 1),
			beforePage : beforePage,
			nextPage : nextPage,
			currentPage : currentPage,
			pageQty : pageQty,
			pageSize : pageSize
		};
		return answer;
	};
	return {
		calculatePagination : calculatePagination
	};
}();