var imperium_ui_view_profile_ChangePassword = function() {
    // "use strict";
    var urlManager = imperium_ui_utils_UrlManager.module;
    
    var module = {
	init : function() {
	    this.setupForms();
	    this.bindToButtons();
	},
	onCancelButton: function(event) {
	    event.preventDefault();
	    window.location = urlManager.getRootUrl();
	},
	bindToButtons: function() {
	  var me = this;
	  $('button[name="cancel"]').on("click", function(event){
	      me.onCancelButton(event);
	  });  
	    
	},
	setupForms : function() {
	    var me = this;

	    var rules = {
		password : {
		    required : true,
		    minlength : 8
		},
		newPassword : {
		    required : true,
		    minlength : 8
		},
		newPasswordAgain : {
		    required : true,
		    minlength : 8,
		    equalTo : "#newPassword"
		}
	    };

	    var passwordMessage = {
		required : "Need input",
		minlength : "It must have at least 8 characters"
	    };
	    var messages = {
		password : passwordMessage,
		newPassword : passwordMessage,
		newPasswordAgain : {
		    required : "Need input",
		    minlength : "It must have at least 8 characters",
		    equalTo : "The passwords do not match"
		}
	    };

	    $("form").validate({
		rules : rules,
		messages : messages,
		errorClass : "text-error"
	    });
	}
    };

    return {
	module : module
    };

}();

jQuery(function($) {
    var module = imperium_ui_view_profile_ChangePassword.module;
    module.init();
});