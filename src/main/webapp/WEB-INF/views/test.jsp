<%@ include file="/WEB-INF/views/fragment/jspHeader.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Imperium application</title>
</head>
<body>
    <p>
        Sample boolean :
        <c:out value="${sessionScope.sampleBoolean}" />
    </p>
    <p>
        Sample value in map :
        <c:out value="${sessionScope.test['boolean']}" />
    </p>
    <p>
        Sample value again in map :
        <c:out value="${test['boolean']}" />
    </p>
    <p>
        <c:if test="${sessionScope.test['boolean']}">It's true</c:if>
    </p>
</body>
</html>