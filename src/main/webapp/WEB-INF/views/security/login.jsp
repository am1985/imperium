<%@ include file="/WEB-INF/views/fragment/jspHeader.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Imperium application</title>
<%@ include file="/WEB-INF/views/fragment/baseCss.jsp"%>
<%@ include file="/WEB-INF/views/fragment/baseJs.jsp"%>
<%@ include file="/WEB-INF/views/fragment/baseWidgets.jsp"%>
<script src="/imperium/resources/app/view/security/Login.js"></script>
</head>
<body>
    <!-- Information from the server -->
    <div
        id="information"
        class="hidden"
        data-from-invalid-login="<c:out value="${FROM_INVALID_CREDENTIALS}"/>"></div>
    <div class="container">
        <div class="row">
            <div class="span4 offset4 well">
                <legend>Please Sign In</legend>
                <div id="errorMessage" class="alert alert-error">
<!--                 Message here -->
<!--                     <a -->
<!--                         class="close" -->
<!--                         data-dismiss="alert" -->
<!--                         href="#">�</a>Incorrect Username or Password! -->
                </div>
                <form
                    method="post"
                    action="/imperium/webapp/security/doLogin"
                    accept-charset="UTF-8">
                    <input
                        type="text"
                        id="username"
                        class="span4"
                        name="username"
                        placeholder="Username">
                        <input
                            type="password"
                            id="password"
                            class="span4"
                            name="password"
                            placeholder="Password">
                            <!--                             <label class="checkbox"> -->
                            <!--                                 <input -->
                            <!--                                     type="checkbox" -->
                            <!--                                     name="remember" -->
                            <!--                                     value="1"> Remember Me  -->
                            <!--                             </label> -->
                            <button
                                type="submit"
                                name="submit"
                                class="btn btn-info btn-block">Sign in</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>