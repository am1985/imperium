Tema:
Desarrollo de una plataforma de control de acceso para aplicaciones

Área de competencia: Desarrollo de software.

Hipótesis de trabajo:

	Al realizar el control de acceso de los recursos de una aplicación se asigna a cada usuario
un rol que posee uno o más permisos .
	Los permisos son el elemento básico de construcción de la política de seguridad y se define para cada uno de ellos el recurso a acceder junto con  una acción a realizar .
	Por otra parte los roles son entidades que agrupan permisos y en sistemas con una gran cantidad de usuarios  facilitan el control de acceso .
	Las empresas frente a la creación de cada producto desarrollan en el mismo la lógica para el control de acceso . La existencia de una plataforma de administración de permisos que maneje esta lógica en forma transparente ahorraría mucho tiempo de desarrollo .
	La plataforma debe cumplir los siguientes objetivos :
1. Fácil de entender.	
2. Flexible y fácil de usar.
3. Extensible y abierta a través de una interfaz de programación de aplicaciones (API).
4.  Open Source (BSD, MIT, Apache License 2.0), para asegurar una mayor adopción y aportes de la comunidad
5. Proveer una interfaz de programación clara y reusable de modo que sea posible de crear un kit de desarrollo (SDK) en cualquier lenguaje de programación .

Objetivo:

	El presente proyecto de tesis propone el desarrollo de una plataforma de control de acceso donde sea posible definir aplicaciones y para cada aplicación un conjunto de recursos y acciones posibles sobre los mismos . Además a los usuarios se les asigna un rol que agrupa uno o más permisos.
	Por otra parte un permiso se define como la combinación entre una acción y un recurso  .
	La plataforma debe poseer una interfaz web a través de la cual se realiza la administración de las aplicaciones . Más específicamente debe permitir :
1. Crear , eliminar , modificar y listar aplicaciones.
2. Crear , eliminar , modificar y listar roles de de una aplicación.
3. Crear , eliminar , modificar y listar los permisos de una aplicación .
4. Crear , eliminar , modificar y listar usuarios de una aplicación.
	Por otro lado va a poseer una interfaz de programación a través de la cual expone la siguiente funcionalidad:
1. Crear , eliminar y modificar usuarios de una aplicación.
2. Consultar roles y permisos disponibles de los usuarios de una aplicación.
	Luego haciendo uso de la interfáz (Api ) se va a desarrollar un kit de desarrollo en Php y  finalmente se va a crear una aplicación de ejemplo .
	La plataforma esta pensada para tres tipos de usuarios distintos:
1. Developers: Se encargan de crear aplicaciones ,  roles base y los permisos.
2. Usuarios : Manjean usuarios y modifican los roles agregando o quitando permisos.
3. Usuarios sdk : Acceden a la funcionalidad a través del kit de desarrollo.

Aporte de este trabajo:

	El principal aporte que pretende este trabajo es la realización de una plataforma de control de acceso que va  a poseer una aplicación web , un webservice rest para exponer  parte de su funcionalidad y un kit de desarrollo en Php . Como consecuencia de esto va a ser posible agregarle control de acceso a cualquier tipo de aplicación en forma rápida y segura .
	
Plan de Trabajo:

El plan de trabajo propuesto es el siguiente:
1. Desarrollo de la aplicación web  (Estimativamente 15 días):
1. Iteración para crear , modificar y listar aplicaciones.
2. Iteración para crear , modificar y listar permisos de una aplicación.
3. Iteración para crear , modificar y listar roles de una aplicación.
4. Iteración para crear , modificar y listar usuarios de una aplicación.
5. Iteración para eliminar aplicaciones , permisos , roles y usuarios.
2. Desarrollo del api( Estimativamente 7 días )
1. Iteración para exponer la funcionalidad de alta , baja y modificación usuarios de una   aplicación.
2. Iteración para expones la funcionalidad de consulta de  roles y permisos de usuarios de una aplicación.
3. Desarrollo del kit de desarrollo (Estimativamente 5 días ):
1. Iteración para exponer la funcionalidad de alta , baja y modificación usuarios de una  aplicación.
2. Iteración para exponer la funcionalidad de consulta de  roles y permisos de usuarios de una aplicación.
4. Desarrollo de la aplicación de ejemplo (Estimativamente 2 días)

Entregables:

Los entregables serán:
1. Una aplicación web para realizar la administración de la plataforma.
2. Una interfaz de programación para acceder a la funcionalidad de la plataforma (API).
3. Un kit de desarrollo que haga uso de la interfaz de programación (SDK).
4. Una aplicación para ejemplificar el uso de la plataforma.
